﻿//check to see if there is a hash appended to the URL - if not, set the to initialLoad to true 
//so it will display home items
var loadHash = $(location).attr('hash');
var sectionListArray = [];
if (!loadHash) {
    //set initialLoad to true (so it will hide all and show default)
    initialLoad = true;
} else {
    initialLoad = false;
}
//collapse mobile menu on click
$(document).on('click', '.navbar-collapse.in', function (e) {
    if ($(e.target).is('a')) {
        $(this).collapse('hide');
    }
});
//once document is ready do this
$(document).ready(function () {
    loadSections();
    //orderAnchors();
    if (initialLoad == 'true') {
        hideSections();
        $('section#home').slideToggle(500);
        initialLoad = false;
        $('section#home').addClass('activeSection');
    } else {
        hideSections();
        $('section' + loadHash).slideToggle(500);
        $('section' + loadHash).addClass('activeSection');
    }
});

//Nav Item Click
$(".nav a").on("click", function () {
    if ($(this).hasClass('dropdown-toggle')) {

    } else {
        $(".nav").find(".active").removeClass("active");
        $(this).parent().addClass("active");
        //get rid of the class from the old activeSection and hide it
        oldActiveSection = $('section.activeSection');
        oldActiveSection.removeClass('activeSection');
        oldActiveSection.hide();
        //add the class to the new activeSection and slide it open
        var newAddressValue = $(this).attr('href');
        $('section' + newAddressValue).addClass('activeSection');
        newActiveSection = $('section.activeSection');
        newActiveSection.slideToggle(500);
    }
});
//Hide All Sections
function hideSections() {
    $('section.hideable-content').hide();
}
//Order Lists
function orderAnchors() {
    var sections = sectionListArray.length;
    var x;
    for (x = 0; x < sections; ++x) {
        $("#" + sectionListArray[x] + " .list-group-item").sort(function (a, b) {
            var itemA = parseInt($(a).attr("data-order"));
            var itemB = parseInt($(b).attr("data-order"));
            return (itemA < itemB) ? -1 : (itemA > itemB) ? 1 : 0;
        }).appendTo($('#' + sectionListArray[x] + ' .list-group'));
    }
}
//Load Sections
function loadSections() {

    //Sections with lists
    $('.nav_pdf_list').each(function (index, obj) { 
        //get the HREF (section)
        var href = $(obj).attr('href');
        var id = href.substring(1);
        var section = 'section' + href;
        var sectionTitle = $(obj).data('grade');
        var sectionParagraph = 'Section: ' + $(obj).data('book-type');
        //Get the files
        var pdfURL = "http://localhost:51370/pdf/" + sectionTitle + "/" + $(obj).data('book-type');
        $.ajax({
            url: pdfURL,
            success: function (data) {
                $(data).find("a:contains(.pdf)").each(function () {
                    //Create Order From PDF Name
                    //0.0_File Name.pdf
                    var pdfTitle = $(this).text();
                    var pdfSection = pdfTitle.substring(0, pdfTitle.indexOf('.'));
                    var pdfOrder = pdfTitle.split('.');
                    pdfOrder = pdfOrder[1];
                    pdfOrder = pdfOrder.split('_');
                    pdfOrder = pdfOrder[0];
                    var pdfPrettyTitle = pdfTitle.substring(pdfTitle.indexOf("_") + 1);

                    var pdfHref = $(this).attr("href");
                    var pdfLink = '<a class="list-group-item" data-order="' + pdfOrder + '" href=' + pdfHref + '>' + pdfOrder + ' : ' + pdfPrettyTitle + '</a>';
                    // will loop through 
                    $(pdfLink).appendTo('section#' + id + ' div.list-group');
                    orderAnchors();
                });
            }
        });

        //create the section on the page
        $('div.jumbotron').append('<section class="hideable-content" id=' + id + '> \
                                <h1>' + sectionTitle + '</h1> \
                                <p>' + sectionParagraph + '</p> \
                                <div class="list-group"> \
                                </div> \
                                </section>');
        sectionListArray.push(id);
    });


    //Sections with 'Units'
    $('.nav_pdf_units').each(function (index, obj) {
        //get the HREF (section)
        var href = $(obj).attr('href');
        var id = href.substring(1);
        var section = 'section' + href;
        var sectionTitle = $(obj).data('grade');
        var sectionParagraph = 'Section: '+$(obj).data('book-type');
        if (sectionTitle == "Kindergarten") {
            var units = 10;
            unitNameArray = ['Families','Friends','Transportation','Food','Animals','Neighborhood','Weather','Plants','Amazing Creatures','I Know A Lot!'];
        } else {
            var units = 6;
        }
        //create the opening of the section
        var sectionString = '<section class="hideable-content" id=' + id + '> \
                                <h1>' + sectionTitle + '</h1> \
                                <p>' + sectionParagraph + '</p> \
                                <div class="container-fluid">';
        //create the units
        var x;
        for (x = 1; x <= units; ++x) {
            var unitArray = [];
            unitArray.push(x);
            if (sectionTitle == "Kindergarten") {
                var unitName = unitNameArray[x - 1];
                var headerString = '<h3>Unit ' + x + ': ' + unitName + '</h3>';
            } else {
                var headerString = '<h3>Unit ' + x + '</h3>';
            }
            var unitString = '<div class="list-group-'+x+' col-xs-12" data-unit="'+x+'"> \
                        '+headerString+' \
                        <div class="unit-group"> \
                        </div> \
                        </div> ';
            sectionString = sectionString + unitString;
        }
        //close the section
        sectionString = sectionString + '</div></section>';
        //append the section to the page
        $('div.jumbotron').append(sectionString);
        //get our files and add them to the sections
        var pdfURL = "http://localhost:51370/pdf/" + sectionTitle + "/" + $(obj).data('book-type');
        $.ajax({
            url: pdfURL,
            success: function (data) {
                $(data).find("a:contains(.pdf)").each(function () {
                    //Create Order From PDF Name
                    var pdfTitle = $(this).text();
                    var pdfUnit = pdfTitle.substring(0, pdfTitle.indexOf('.'));
                    var pdfOrder = pdfTitle.split('.');
                    pdfOrder = pdfOrder[1];
                    pdfOrder = pdfOrder.split('_');
                    pdfOrder = pdfOrder[0];
                    var pdfPrettyTitle = pdfTitle.substring(pdfTitle.indexOf("_") + 1);
                    var pdfHref = $(this).attr("href");
                    var pdfLink = '<a class="list-group-item" href=' + pdfHref + '>' + pdfPrettyTitle + '</a>';
                    // will loop through 
                    $(pdfLink).appendTo('section#' + id + ' div.list-group-'+pdfUnit+'');
                });
            }
        });
    });
    //Sections with 'Units + Subunits' (unique anthologies for 1st, 2nd and 3rd grade)
    $('.nav_pdf_unique_anthology').each(function (index, obj) {
        //get the HREF (section)
        var href = $(obj).attr('href');
        var id = href.substring(1);
        var section = 'section' + href;
        var sectionTitle = $(obj).data('grade');
        var sectionParagraph = 'Section: ' + $(obj).data('book-type');
        var firstGradeAnthologies = ['All About Us', 'Outside My Door', "Let's Connect", 'Nature Watch', 'Adventure All Around', "Let's Discover"];
        var secondGradeAnthologies = ['Seahorse', 'Dragonfly'];
        var thirdGradeAnthologies = ['Fox', 'Peacock'];

        if (sectionTitle == "First Grade") {
            var units = 6;
            //setup SubCategories
            var subCategories = 5;
            var subCategoryArrays = [['We Are Special', 'Ready Set Move', 'Growing Up', 'Pets', 'Teamwork'],
                                         ['Animal Families', 'Helping Out', 'Where Animals Live', 'Sing and Dance', "Let's Laugh"],
                                         ['Being Friends','Kids Around The World','Me and My Shadow','Our Families','Family Time'],
                                         ['Birds','Recycling',"What's the Weather",'What Scientists Do','Favorite Stories'],
                                         ['Express Yourself', 'Watch It Go', 'Inventions', 'I Can Do It', 'How Does It Grow'],
                                         ['Bugs, Bugs, Bugs','Exploring Space','At Work','Watching Animals Grow',"Let's Build"]
                                        ];
        } else {
            var units = 2;
            var subCategories = 6;
        }
        //create the opening of the section
        var sectionString = '<section class="hideable-content" id=' + id + '> \
                                <h1>' + sectionTitle + '</h1> \
                                <p>' + sectionParagraph + '</p> \
                                <div class="container-fluid">';
        //create the sections
        var x;
        for (x = 1; x <= units; ++x) {
            var unitArray = [];
            unitArray.push(x);

            if (sectionTitle == "First Grade") {
                var unitName = firstGradeAnthologies[x - 1];
                subSectionString = '<div class="container-fluid">'
                var y;
                for (y = 0; y < subCategories; ++y) {
                    var subCategoryName = subCategoryArrays[x-1][y];
                    var headerString = '<h3>Section ' + x + ': ' + unitName + '</h3>';
                    var subCategoryString = '<div class="sub-list-group-' + (y+1) +' group-' + (y+1) + ' subcategories col-xs-12"><h4>' + subCategoryName + '</h4></div>';
                    subSectionString = subSectionString + subCategoryString;
                }
                subSectionString = subSectionString + '</div>';
            }

            else if (sectionTitle == "Second Grade") {
                var unitName = secondGradeAnthologies[x - 1];
                subSectionString = '<div class="container-fluid">'
                var y;
                for (y = 0; y < subCategories; ++y) {
                    var subCategoryName = y+1;
                    var headerString = '<h3>Section ' + x + ': ' + unitName + '</h3>';
                    var subCategoryString = '<div class="sub-list-group-' + (y+1) + ' group-' + (y + 1) + ' subcategories col-xs-12"><h4>' + subCategoryName + '</h4></div>';
                    subSectionString = subSectionString + subCategoryString;
                }
                subSectionString = subSectionString + '</div>';
            }

            else if (sectionTitle == "Third Grade") {
                var unitName = thirdGradeAnthologies[x - 1];
                subSectionString = '<div class="container-fluid">'
                var y;
                for (y = 0; y < subCategories; ++y) {
                    var subCategoryName = y + 1;
                    var headerString = '<h3>Section ' + x + ': ' + unitName + '</h3>';
                    var subCategoryString = '<div class="sub-list-group-' + (y + 1) + ' group-' + (y + 1) + ' subcategories col-xs-12"><h4>Unit: ' + subCategoryName + '</h4></div>';
                    subSectionString = subSectionString + subCategoryString;
                }
                subSectionString = subSectionString + '</div>';
            } else {
                var headerString = '<h3>Section ' + x + '</h3>';
            }
            var unitString = '<div class="list-group-' + x + ' col-xs-12" data-unit="' + x + '"> \
                        '+ headerString + ' \
                        '+ subSectionString +' \
                        </div> ';
            sectionString = sectionString + unitString;
        }
        //close the section
        sectionString = sectionString + '</div></section>';
        //append the section to the page
        $('div.jumbotron').append(sectionString);
        //get our files and add them to the sections
        var pdfURL = "http://localhost:51370/pdf/" + sectionTitle + "/" + $(obj).data('book-type');
        $.ajax({
            url: pdfURL,
            success: function (data) {
                $(data).find("a:contains(.pdf)").each(function () {
                    //Create Order From PDF Name
                    var pdfTitle = $(this).text();
                    var orders = pdfTitle.split('.');
                    //0.0.1_FILENAME.pdf;
                    var pdfSection = orders[0];
                    //list-group-1
                    var pdfUnit = orders[1];
                    var pdfOrder = pdfTitle.split('.');
                    pdfOrder = pdfOrder[1];
                    pdfOrder = pdfOrder.split('_');
                    pdfOrder = pdfOrder[0];
                    var pdfPrettyTitle = pdfTitle.substring(pdfTitle.indexOf("_") + 1);
                    var pdfHref = $(this).attr("href");
                    var pdfLink = '<a class="list-group-item" href=' + pdfHref + '>' + pdfPrettyTitle + '</a>';
                    // will loop through 
                    $(pdfLink).appendTo('section#' + id + ' div.list-group-' + pdfSection + ' div.sub-list-group-' + pdfUnit + '');
                });
            }
        });
    });
}
